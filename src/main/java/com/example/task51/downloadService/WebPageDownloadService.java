package com.example.task51.downloadService;

import com.example.task51.counterService.wrapper.WebPage;
import org.springframework.stereotype.Component;

@Component
public interface WebPageDownloadService {
    WebPage download();
}

