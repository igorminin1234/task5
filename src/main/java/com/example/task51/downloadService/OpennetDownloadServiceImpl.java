package com.example.task51.downloadService;

import com.example.task51.counterService.wrapper.WebPage;
import com.example.task51.counterService.wrapper.Link;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Profile("opennet")
public class OpennetDownloadServiceImpl extends AbstractDownloadService {

    private final Link link;

    public OpennetDownloadServiceImpl(@Value("${link}") final String address) {
        this.link = new Link(address);
    }
    @Override
    public WebPage download() {
        try {
            return getHTMLFromResponse(link.receiveLink());
        } catch (IOException | InterruptedException e) {

            e.printStackTrace();
        }
        return null;
    }
}