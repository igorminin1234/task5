package com.example.task51;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NewsPrintingService {

    private final String link;

    public NewsPrintingService(@Value("${link}") final String link) {
        this.link = link;
    }

    public void printNewsCount(final int numberOfArticles) {
        System.out.println("The amount of today's news on " + link + " is " + numberOfArticles);
    }
}

