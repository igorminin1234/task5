package com.example.task51.counterService;

import com.example.task51.counterService.wrapper.WebPage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractCounterService implements NewsCounterService {

/*    private int counting(final int counter, final Matcher match) {
        if (match.find()) {
            counting(counter + 1, match);
        }
        return counter;
    }*/

    protected WebPage countNumberOfArticlesPerPage(final String regexp, final WebPage page) {
        if (page.receiveDownloadedPage() == null) {
            return null;
        }
        final Pattern pat = Pattern.compile(regexp);
        final Matcher match = pat.matcher(page.receiveDownloadedPage());
        WebPage counter = new WebPage(page.receiveDownloadedPage(), 0);
        while (match.find()) {
            counter = new WebPage(page.receiveDownloadedPage(), counter.receiveNumberOfArticles() + 1);
        }
        return counter;
        //return counting(0, match);
    }

}