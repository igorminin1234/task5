package com.example.task51.counterService;

import com.example.task51.counterService.wrapper.WebPage;
import org.springframework.stereotype.Component;

@Component
public interface NewsCounterService {
    WebPage count(final WebPage html);
}