package com.example.task51.counterService.wrapper;

public class Regexp {
    private final String pattern;

    public Regexp(final String pattern) {
        this.pattern = pattern;
    }

    public String receiveRegexp() {
        return pattern;
    }
}
