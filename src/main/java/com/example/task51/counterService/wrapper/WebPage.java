package com.example.task51.counterService.wrapper;

public class WebPage {

    private final String html;

    private final int numberOfArticles;

    public WebPage(final String html) {
        this.html = html;
        this.numberOfArticles=0;
    }

    public WebPage(final String html, final int numberOfArticles) {
        this.html = html;
        this.numberOfArticles = numberOfArticles;
    }

    public String receiveDownloadedPage() {
        return html;
    }

    public int receiveNumberOfArticles() {
        return numberOfArticles;
    }
}
