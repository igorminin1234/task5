package com.example.task51.counterService.wrapper;

public class Link {

    private final String address;

    public Link(final String address) {
        this.address = address;
    }

    public String receiveLink() {
        return address;
    }
}
