package com.example.task51.counterService.wrapper;

public class DateFormat {

    private final String format;

    public DateFormat(final String format) {
        this.format = format;
    }

    public String receiveDateFormat() {
        return format;
    }
}
