package com.example.task51.counterService;

import com.example.task51.counterService.wrapper.Regexp;
import com.example.task51.counterService.wrapper.WebPage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("mirknig")
public class MirknigNewsCounterServiceImpl extends AbstractCounterService {


    private final Regexp regexp;

    public MirknigNewsCounterServiceImpl(@Value("${today.pattern}") final String pattern) {
        this.regexp = new Regexp(pattern);
    }

    @Override
    public WebPage count(final WebPage page) {

        return countNumberOfArticlesPerPage(regexp.receiveRegexp(), page);
    }
}


