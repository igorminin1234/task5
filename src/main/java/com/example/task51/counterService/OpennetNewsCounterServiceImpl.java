package com.example.task51.counterService;

import com.example.task51.counterService.wrapper.DateFormat;
import com.example.task51.counterService.wrapper.Regexp;
import com.example.task51.counterService.wrapper.WebPage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
@Profile("opennet")
public class OpennetNewsCounterServiceImpl extends AbstractCounterService {

    private final Regexp regexp;
    private final DateFormat dateFormat;

    public OpennetNewsCounterServiceImpl(@Value("${today.pattern}") final String pattern, @Value("${date.format}") final String format) {
        this.dateFormat = new DateFormat(format);
        this.regexp = new Regexp(pattern);
    }

    private String changeDateFormat(final LocalDate startDateFormat) {
        return new SimpleDateFormat(dateFormat.receiveDateFormat())
                .format(Date.from(startDateFormat.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public WebPage count(final WebPage page) {

        return countNumberOfArticlesPerPage(
                regexp.receiveRegexp().replaceAll("changeFormatDate", changeDateFormat(LocalDate.now()).replaceAll("\\.", "\\.")) //replace for change date format from yyyy-mm-dd to dd.mm.yyyy
                , page
        );
    }
}
