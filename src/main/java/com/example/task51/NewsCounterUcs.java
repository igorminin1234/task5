package com.example.task51;

import com.example.task51.counterService.NewsCounterService;
import com.example.task51.counterService.wrapper.WebPage;
import com.example.task51.downloadService.WebPageDownloadService;
import org.springframework.stereotype.Component;

@Component
public class NewsCounterUcs {
    final WebPageDownloadService webPageDownloadService;
    final NewsCounterService newsCounterService;
    final NewsPrintingService newsPrintingService;

    public NewsCounterUcs(final WebPageDownloadService webPageDownloadService, final NewsCounterService newsCounterService, final NewsPrintingService newsPrintingService) {
        this.webPageDownloadService = webPageDownloadService;
        this.newsCounterService = newsCounterService;
        this.newsPrintingService = newsPrintingService;
    }

    public void run() {
        WebPage page = webPageDownloadService.download();
        page = newsCounterService.count(page);
        newsPrintingService.printNewsCount(page.receiveNumberOfArticles());
    }
}